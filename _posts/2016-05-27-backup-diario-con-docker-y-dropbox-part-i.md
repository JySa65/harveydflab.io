---
layout: post
comments: true
title:  Backup diario con docker y dropbox - I
date:   2016-05-27 13:00:00 +0100
categories: docker django
permalink: /backup-diario-con-docker-y-dropbox-part-i/
---
Docker es una tecnología que está en auge, por su "facilidad de uso", por su portabilidad, por ser multi plataforma, etc. Tiene grandes ventajas que nos ayudan en el desarrollo de nuestras aplicaciones.

Una de esas es tener un motor de base de datos, llámese Postgresql o MySQL, configurado y corriendo en un par de minutos, sin tener que batallar con apt-get install, brew install, y ni idea cómo será en Windows.

Para los que ya han trabajo con docker la siguiente sección es algo obvia pero la quiero mencionar para explicar el porqué de este tutorial (algo que necesité aprender precisamente para este blog). Si quieres puedes saltar hasta la sección de **Requisitos**.

## Filosofía

Dos de las filosofías de docker y sus contenedores que quiero citar son estas:

1. **Los contenedores debería correr un sólo proceso**. Es decir, un sólo contenedor nunca debería correr django, postgres, redis y nginx. Por el contrario deberíamos tener un contenedor para django otro para postgres y así sucesivamente.

2. **Los contenedores son efímeros**. Eso quiere decir que deberíamos crear y borrar contenedores a nuestro antojos sin problema, por lo tanto data que queremos que sea permanente no debería estar solamente en el contenedor, porque al borrarlo esta data se pierde con él.

Para resolver el punto 2 docker desarrollo algo llamado **volúmenes**, que es como una especie de disco duro o carpeta dentro del _host_ en dónde esa data es guardada y permanece ahí aunque el contenedor se destruya.

Pero ¿qué pasa si tenemos que destruir nuestro _docker machine_? Entonces también perdemos nuestra data, al igual que si tuviéramos que borrar una instancia en _AWS_ o un _droplet_ en DigitalOcean.

Así que sea que trabajemos con docker o no es una buena práctica hacer **_backups_** de nuestra data, en este caso nuestra base de datos, regularmente y guardar esos archivos en alguna parte fuera de los servidores.

## Requisitos
En este caso vamos a usar lo siguiente:

* Aplicación en dropbox para subir las copias.
* Una base de datos en postgres.
* Un script en bash para generar los _backups_.
* Un _cron job_ para hacer las copias periódicamente.

### Dropbox

Para poder subir los archivos a dropbox a través de su API necesitamos crear un **aplicación** y generar un **_access token_**. Suena más complejo de lo que realmente es (a diferencia de otra APIs como por ejemplo Facebook).

Para crear nuestra app vamos a [https://www.dropbox.com/developers/apps](https://www.dropbox.com/developers/apps) y le damos click en el botón de **crear app**.

![crear app]({{ site.url }}/assets/img/posts/crear-app.png)

Después seleccionamos **Dropbox API**. Para el tipo de acceso seleccionamos **app folder**, esto creará una carpeta dentro de nuestro dropbox llama **Aplications/[app name]/**. Por último le damos un **nombre** a nuestra app y le damos click al botón de **crear app**.

![settings]({{ site.url }}/assets/img/posts/settings.png)

Así como está la aplicación es suficiente para lo que queremos hacer, ahora sólo necesitamos nuestro **access token**. En la sección de OAuth2 damos click en el botón de **Generate**. Esto nos dará como resultado un token que debemos copiar y guardar porque dropbox no lo volverá a mostrar. Siempre podemos generar otro access token, pero con uno es suficiente.

![token]({{ site.url }}/assets/img/posts/token.png)

### Base de datos
Para la parte de la base de datos vamos a necesitar tener [docker](https://docs.docker.com/engine/installation/) y [docker-compose](https://docs.docker.com/compose/install/) instalados.

Creemos una carpeta, que va a ser nuestro _root_, donde vamos a crear todo ahí, por ejemplo:

{% highlight console %}
$ ~/Tutoriales/docker-backup && cd $_
{% endhighlight %}

Luego creamos nuestro archivo _docker-compose.yml_

{% highlight yaml %}
version: "2"

services:
  db:
    image: postgres
    environment:
      - POSTGRES_USER=myuser
      - POSTGRES_PASSWORD=secret
      - POSTGRES_DB=mydb
{% endhighlight %}

Nota: En este ejemplo (para no alargar más el tutorial) no estoy usando ningún volumen para hacer la BD persistente. Si quieres saber más sobre eso puedes referirte a la [documentación de docker](https://docs.docker.com/engine/userguide/containers/dockervolumes/).

En este archivo es donde normalmente irán nuestros servicios como django, node, nginx, redis, etc.

Si corremos `docker-compose up` tendremos corriendo nuestra base de datos usando la imagen oficial de postgres en docker. Nota: las líneas del 7-9 son variables de entorno para configurar ls BD. Para mayor referencia sigue el enlace [Environment Variables
](https://hub.docker.com/_/postgres/)

Hasta aquí estamos listos para entrar en materia. En la [segunda parte](/backup-diario-con-docker-y-dropbox-part-ii) de este post veremos el docker-compose y el script para generar las copias y subirlas a dropbox.