---
layout: post
comments: true
title:  Backup diario con docker y dropbox - II
date:   2016-05-27 14:00:00 +0100
categories: docker django
permalink: /backup-diario-con-docker-y-dropbox-part-ii/
---
En la [primera parte](/backup-diario-con-docker-y-dropbox-part-i) de este tutorial vimos como crear la aplicación en dropbox y el docker-compose de la base de datos.

### Container
Vamos a crear el segundo contenedor que se va a encargar de hacer los respaldos y subirlos a dropbox.

Para eso editamos nuestro archivo **_docker-compose.yml_**

{% highlight yaml %}
version: "2"

services:
  db:
    image: postgres
    environment:
      - POSTGRES_USER=myuser
      - POSTGRES_PASSWORD=secret
      - POSTGRES_DB=mydb

  backup:
    build: .
    environment:
      - POSTGRES_USER=myuser
      - POSTGRES_PASSWORD=secret
      - POSTGRES_DB=mydb
      - POSTGRES_HOST=db
      - DROPBOX_ACCESS_TOKEN=4cc3sst0k3nd3dr0pb0x
{% endhighlight %}


Lo que estamos diciendo es que queremos construir nuestra propia imagen de docker. Nos basaremos en la de postgres pero agregaremos un par de cosas adicionales. 

Las líneas de *environment* están repetidas deliberadamente para que sea fácil de leer, pero se puede usar un [environment file](https://docs.docker.com/compose/compose-file/#env-file) para evitar duplicar las variables de entorno.

La variable **POSTGRES_HOST** es el nombre del contenedor de la base de datos, en este caso es **db**.

Para poder crear la imagen necesitamos un archivo llamado ***Dockerfile***

{% highlight docker %}
# Basamos nuestra imagen en la postgres
# esto es importante porque necesitamos el
# comando de pg_dump que viene en dicha imagen
FROM postgres

# Actualizamos los repositorios e instalamos curl
RUN apt-get update && apt-get -y install curl

# Copiamos el script que hace los backups en el root 
COPY backup.sh /

# Copiamos el archivo con el cronjob en /etc/cron.d/
COPY cron-backup /etc/cron.d/

# Creamos el archivo de log para el cron
RUN touch /var/log/cron.log

# Ejecutamos el comando cron para que corra el cron job
# dentro de cron-backup
# El comando tail nos ayuda a mantener vivo el contenedor
# y que no salga despues de ejecutar "cron"
CMD cron && tail -f /var/log/cron.log
{% endhighlight %}

Como vemos, nos hacen falta dos archivos, el script y el cron. Sigamos con eso.

### Script
Este script es el que se va a encargar de generar el **_dump_** y de subirlo a **dropbox**. Lo llamaremos **_backup.sh_**

{% highlight bash %}
#!/bin/bash

# Este comando hace que si hay un error el script
# llame un exit con el n√mero del error y no siga
# ejecutando
set -e

# Capturamos la fecha actual en formato yyyy-mm-dd
today=$(date +%Y-%m-%d)

# Generamos el nombre que va a tener el archivo sql
# por ejemplo mydb-2016-09-29.sql
# la variable $POSTGRES_DB la toma de las variables de entorno
# que pasamos en el docker-compose.yml
filename="$POSTGRES_DB-$today.sql"

# Generamos el dump de la base de datos y lo guardamos en un archivo
# con el nombre que definimos antes
pg_dump -h $POSTGRES_HOST -U $POSTGRES_USER $POSTGRES_DB > /tmp/$filename

# Para subir un archivo a dropbox basta con hacer un POST a la API url
# con un access token
curl -X POST https://content.dropboxapi.com/2/files/upload \
	-H "Authorization: Bearer $DROPBOX_ACCESS_TOKEN" \
	-H "Dropbox-API-Arg: {\"path\": \"/$filename\", \"mode\": \"add\",\"autorename\": true,\"mute\": false}" \
	-H "Content-Type: application/octet-stream" \
	--data-binary @/tmp/$filename
{% endhighlight %}

Nota: El comando de curl lo tomamos basado en la [documentación de dropbox.](https://www.dropbox.com/developers/documentation/http/documentation#files-upload)

Algo muy importante que no se nos puede olvidar a la hora de crear estos scripts (y créanme, con docker van a ser bastantes) es darle permiso de ejecución.

{% highlight yaml %}
$ chmod +x backup.sh
$ ls -l backup.sh
-rwxr-xr-x  1 harveydf  staff  1020 May 26 22:40 backup.sh
{% endhighlight %}

Esas equis (x) quiere decir que ahora tiene permisos de ejecución.

### Cron
{% highlight bash %}
# La línea en blanco al final es requerido
# para que sea un archivo cron válido
0 0 * * * root ./backup.sh >> /var/log/cron.log 2>&1
{% endhighlight %}

Con esto le decimos a cron que queremos correr el script *backup.sh* todos los días a media noche (00:00) y que el resultado se guarde en el archivo `/var/log/cron.log` y que ese mismo resultado lo envíe a la error estándar `/dev/stderr/` y salida estándar `/dev/stdout/` respectivamente. Para mayor información sobre los *cron jobs* pueden empezar con la [Wikipedia.](https://en.wikipedia.org/wiki/Cron)

### Password
Estamos prácticamente listos pero postgres, a diferencia de mysql, **no** permite pasar el password dentro del comando **pg_dump** sino que debemos crear un archivo oculto llamado **pgpass**. Para mayor información en la [documentación de postgres.](https://www.postgresql.org/docs/current/static/libpq-pgpass.html)

Para generar este archivo nos vamos apoyar en otra utilidad de docker que es el [entrypoint.](https://docs.docker.com/compose/compose-file/#entrypoint) En resumidas cuentas es un script que se ejecuta cada vez que el container se levanta y antes que el proceso principal se ejecute. Es muy útil a la hora de configurar nuestro contenedor antes de lanzar gunicorn, node, gulp, etc.

Creamos un archivo llamado ***docker-entrypoint.sh***

{% highlight bash %}
#!/bin/bash

set -e

# la estructura del archivo es hostname:port:database:username:password
echo "$POSTGRES_HOST:5432:$POSTGRES_DB:$POSTGRES_USER:$POSTGRES_PASSWORD" > ~/.pgpass

chmod 600 ~/.pgpass

exec "$@"
{% endhighlight %}

Nota: recordemos darle permisos de ejecución a este script

Y por último cambiamos el **Dockerfile** para incluir el entrypoint

{% highlight docker %}
# Basamos nuestra imagen en la postgres
# esto es importante porque necesitamos el
# comando de pg_dump que viene en dicha imagen
FROM postgres

# Actualizamos los repositorios e instalamos curl
RUN apt-get update && apt-get -y install curl

# Copiamos el script que hace los backups en el root 
COPY backup.sh /

# Copiamos el archivo con el cronjob en /etc/cron.d/
COPY cron-backup /etc/cron.d/

# Creamos el archivo de log para el cron
RUN touch /var/log/cron.log

# Copiamos el script de configuracion en el root
COPY docker-entrypoint.sh /

# Usamos el entrypoint
ENTRYPOINT ["/docker-entrypoint.sh"]

# Ejecutamos el comando cron para que corra el cron job
# dentro de cron-backup
# El comando tail nos ayuda a mantener vivo el contenedor
# y que no salga despues de ejecutar "cron"
CMD cron && tail -f /var/log/cron.log
{% endhighlight %}

### Conclusión
Antes de terminar no se nos olvide generar las imagen de backup `docker-compose build` de nuevo para que tome los cambios hechos en el **Dockerfile**.

Levantamos los contenedores con `docker-compose up -d` y listo, tenemos nuestra base de datos corriendo y un contenedor que se encarga de hacer backup de la base de datos todos los días a media noche y subir ese respaldo a dropbox.

Si no quieren esperar a media noche para ver si funciona, pueden probar el script con `run`

{% highlight console %}
$ docker-compose run backup ./backup.sh
{
  "name": "mydb-2016-05-26.sql",
  "path_lower": "/mydb-2016-05-26.sql",
  "path_display": "/mydb-2016-05-26.sql",
  "id": "id:q3YHmrx2GfAAAAAAAAAAAQ",
  "client_modified": "2016-05-26T05:14:33Z",
  "server_modified": "2016-05-26T05:14:33Z",
  "rev": "3490c951e", "size": 852
}
{% endhighlight %}

![final]({{ site.url }}/assets/img/posts/final.png)

Ahora el reto que les queda es hacer un script que descargue un archivo de dropbox y haga el restore a la base de datos.

Todo el código del tutorial lo pueden encontrar en el [repositorio de github.](https://github.com/harveydf/tutoriales-backup-dropbox)

Si les gustaría algún tutorial sobre algo que vimos (por ejemplo volúmenes en docker) déjenmelo saber en los comentarios. Igualmente cualquier aporte, crítica, comentario y/o duda la pueden dejar en los comentarios.

Hasta una próxima.